![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# 3D object detection cluttered scene VPFR
This is a type of vision pipeline.  
This type starts an instance of a vision pipeline.  
The instance then loads all algorithms that match the type.  
This type processes images and searches for objects in the images.  
When all object have been found, it is written in the output topic.
 


Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/types/3d-object-detection-cluttered-scene-vpfr/-/wikis/home)
