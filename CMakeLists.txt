cmake_minimum_required(VERSION 3.0.2)
project(vpfr3dobjectdetectionclutteredscene)

#[[
vpfr-3d-object-detection-cluttered-scene
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
SPDX-License-Identifier: MIT
]]

find_package(catkin REQUIRED COMPONENTS
  message_generation
  rospy
  std_msgs
  sensor_msgs
  geometry_msgs
  vision_pipeline
)
catkin_python_setup()

add_message_files(
    FILES
    Single3DObject.msg
    Single3DObjectList.msg
)

generate_messages(
   DEPENDENCIES
   std_msgs
   sensor_msgs
   geometry_msgs
)

catkin_package(
   CATKIN_DEPENDS message_runtime std_msgs vision_pipeline sensor_msgs geometry_msgs
)

catkin_install_python(PROGRAMS src/VpfrNode.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION})

include_directories(
  ${catkin_INCLUDE_DIRS}
)

